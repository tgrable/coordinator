//
//  RootViewController.swift
//  Coordinators
//
//  Created by Tim Grable on 6/4/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    override func viewDidAppear(_ animated: Bool) {
        print("Root: ViewController")
    }
}
