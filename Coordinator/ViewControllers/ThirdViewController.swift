//
//  ThirdViewController.swift
//  Coordinators
//
//  Created by Tim Grable on 6/5/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
    override func viewDidAppear(_ animated: Bool) {
        print("Presented: ThirdViewController")
    }
}
