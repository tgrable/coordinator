//
//  AppCoordinator.swift
//  Coordinators
//
//  Created by Tim Grable on 6/4/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit

class AppCoordinator : BaseCoordinator {

    let window: UIWindow?

    init(scene: UIWindowScene) {
        window = UIWindow(frame: scene.coordinateSpace.bounds)
        window?.windowScene = scene

        super.init()
    }

    override func start() {
        let firstCoordinator = FirstCoordinator()

        self.store(coordinator: firstCoordinator)
        firstCoordinator.start()
        
        window?.rootViewController = firstCoordinator.rootController
        window?.makeKeyAndVisible()

        firstCoordinator.isCompleted = { [weak self] in
            self?.free(coordinator: firstCoordinator)
        }
    }
}
