//
//  Coordinator.swift
//  Coordinators
//
//  Created by Tim Grable on 6/4/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var rootController: UIViewController { get set }
    var activeControllers: [UIViewController] { get set }
    var childCoordinators : [Coordinator] { get set }
    
    func start()
    func setRoot(controller: UIViewController)
    func present(controller: UIViewController)
}

extension Coordinator {

    func store(coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    func free(coordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0 !== coordinator }
    }
    
    func setRoot(controller: UIViewController) {
        rootController = controller
        activeControllers = [rootController]
    }
    
    func present(controller: UIViewController) {
        guard let presentedController = activeControllers.last else { return }
        presentedController.present(controller, animated: true, completion: nil)
        activeControllers.append(controller)
    }
    
    func dismiss(controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
        _ = activeControllers.popLast()
    }
}
