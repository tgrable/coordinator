//
//  BaseCoordinator.swift
//  Coordinators
//
//  Created by Tim Grable on 6/4/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit

class BaseCoordinator : Coordinator {
    
    var rootController: UIViewController = UIViewController()
//    var presentedController: UIViewController = UIViewController()
    var activeControllers: [UIViewController] = []
    var childCoordinators : [Coordinator] = []
    var isCompleted: (() -> ())?

    func start() {
        fatalError("Children should implement `start`.")
    }
}
