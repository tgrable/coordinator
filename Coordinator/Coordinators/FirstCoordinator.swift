//
//  FirstCoordinator.swift
//  Coordinators
//
//  Created by Tim Grable on 6/4/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit

class FirstCoordinator : BaseCoordinator {
    
    override func start() {
        launchFirstViewController()
    }
    
    func launchFirstViewController() {
        let viewController = RootViewController()
        viewController.view.backgroundColor = .red
        setRoot(controller: viewController)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            self.presentSomething()
        })
    }
    
    func presentSomething() {
        let secondViewController = SecondViewController()
        secondViewController.view.backgroundColor = .green
        present(controller: secondViewController)

        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            self.presentSomethingElse()
        })
    }
   
    func presentSomethingElse() {
        let thirdViewController = ThirdViewController()
        thirdViewController.view.backgroundColor = .blue
        present(controller: thirdViewController)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            self.dismiss(controller: thirdViewController)
        })
    }
}
